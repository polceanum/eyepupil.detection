# Eye pupil detection

* expects a small sized image of one face (crop)
* detects eye regions
* calculates most likely pupil location for each detected eye region

# Compilation

```
#!bash
git clone https://bitbucket.org/polceanum/eyepupil.detection.git
cd eyepupil.detection
mkdir build
cd build
cmake ..
make -j
```