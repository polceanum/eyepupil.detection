#include <vector>
#include <string>
#include <iostream>
#include <chrono>
#include <fstream>

#include "opencv2/opencv.hpp"

//#define USE_CAMERA 1
#define DEBUG_DISPLAY 1

std::string intToString(int n)
{
  std::string result = "";
  if (n < 10) result += "00000";
  else if (n < 100) result += "0000";
  else if (n < 1000) result += "000";
  else if (n < 10000) result += "00";
  else if (n < 100000) result += "0";

  result += std::to_string(n);
  return result;
}

int main(int argc, char** argv)
{
    if (argc != 3)
    {
        std::cout << "Expected 2 parameters: rootFolder, outFile" << std::endl;
        return 0;
    }

    std::string rootFolder = "";
    std::string outFile = "";
    for (int i = 0; i < argc; ++i)
    {
        if (i==1)
        {
            rootFolder = std::string(argv[i]);
            std::cout << "Image root folder: " << rootFolder << std::endl;
        }
        else if (i==2)
        {
            outFile = std::string(argv[i]);
            std::cout << "Output filename: " << outFile << std::endl;
        }
    }

    std::ofstream myfile;
    myfile.open(outFile);

    myfile << "frameNr" << ", " << "x1" << ", " << "y1" << ", " << "width1" << ", " << "height1" << ", " << "x2" << ", " << "y2" << ", " << "width2" << ", " << "height2" << "\n";

    #ifdef USE_CAMERA
        cv::VideoCapture capture(-1);
        if( !capture.isOpened() ) return -1;
    #endif

    #ifdef DEBUG_DISPLAY
        cv::namedWindow("Original",1);
        cv::namedWindow("Grey",1);
        cv::namedWindow("Processed",1);
    #endif

    cv::CascadeClassifier cascade;

    if( !cascade.load("../haar/haarcascade_eye_tree_eyeglasses.xml") )
    {
        std::cerr << "ERROR: Could not load classifier cascade" << std::endl;
        return -1;
    }

    std::vector<cv::Rect> objects;
    cv::Scalar color = cv::Scalar(0,0,255);

    const int coordSize = 8;
    float coords[coordSize]; // x, y, width, height for 2 detected eyes (may not be in order)

    cv::Mat frame, grey, result;

    for (int i=1; ; ++i)
    {
        //auto t0 = std::chrono::high_resolution_clock::now();

        if (i%1000 == 0)
        {
            std::cerr << "Progress: " << i << " images" << std::endl;
        }

        #ifdef USE_CAMERA
            capture >> frame;
        #else
            try
            {
                frame = cv::imread(rootFolder+"/frame_det_"+intToString(i)+".bmp");
                //frame = cv::imread("/home/mike/Work/libs/OpenFace/build/bin/00020_crop/frame_det_"+intToString(i)+".bmp");
                //frame = cv::imread("/home/mike/Work/libs/OpenFace/build/bin/00046_crop/frame_det_"+intToString(i)+".bmp");
                //frame = cv::imread("/home/mike/Work/libs/OpenFace/build/bin/00003_crop/frame_det_"+intToString(i)+".bmp");

                #ifndef USE_CAMERA
                if (frame.empty())
                {
                    std::cerr << "Total number of frames processed: " << (i-1) << std::endl;
                    myfile.close();
                    return 0;
                }
                #endif
            }
            catch(cv::Exception& e)
            {
                std::cerr << "Total number of frames processed: " << (i-1) << std::endl;
                myfile.close();
                return 0;
            }
            
        #endif

        #ifdef DEBUG_DISPLAY
            cv::imshow("Original", frame);
        #endif

        cvtColor( frame, grey, CV_BGR2GRAY );   // get a new frame from   camera

        #ifdef DEBUG_DISPLAY
            cv::imshow("Grey", grey);
        #endif

        #ifdef DEBUG_DISPLAY
            result = frame.clone();
        #endif

        objects.resize(0);
        cascade.detectMultiScale( grey, objects,
                                 1.04, 6, 0
                                 //|cv::CASCADE_FIND_BIGGEST_OBJECT
                                 //|cv::CASCADE_DO_ROUGH_SEARCH
                                 |cv::CASCADE_SCALE_IMAGE,
                                 cv::Size(10, 10),
                                 cv::Size(30, 30)
                                 );

        //reset coords
        coords[0] = 0.0f;
        coords[1] = 0.0f;
        coords[2] = 1.0f;
        coords[3] = 1.0f;
        coords[4] = 0.0f;
        coords[5] = 0.0f;
        coords[6] = 1.0f;
        coords[7] = 1.0f;

        //for (size_t obj=0; obj<objects.size(); ++obj)
        for (size_t obj=0; obj<3 && obj<objects.size(); ++obj) // only looking for 2 eyes
        {
            cv::rectangle( result, objects[obj], color, 1, 8, 0 );

            int vMargin = 6;
            int hMargin = 4;
            cv::Rect cropCrop = objects[obj];
            cropCrop.x += hMargin;
            cropCrop.y += vMargin;
            cropCrop.width -= hMargin*2;
            cropCrop.height -= vMargin*2;

            #ifdef DEBUG_DISPLAY
                cv::rectangle(result, cv::Rect(objects[obj].x+hMargin, objects[obj].y+vMargin, cropCrop.width, cropCrop.height), color, 1, 8, 0 );
            #endif

            cv::Mat eyeCrop = grey(cropCrop);

            cv::equalizeHist( eyeCrop, eyeCrop ); // improves contrast

            #ifdef DEBUG_DISPLAY
                cv::imshow("eye crop", eyeCrop );
            #endif

            float meanX = 0.0f;
            float meanY = 0.0f;
            int divBy = 0;

            for (size_t x=0; x<eyeCrop.rows; x++)
            {
                for (size_t y=0; y<eyeCrop.cols; ++y)
                {
                    int crtVal = (int)eyeCrop.at<uchar>(y,x);
                    if (crtVal <= 10)
                    {
                        meanX += x;
                        meanY += y;
                        divBy++;
                    }
                }
            }

            if (divBy > 0)
            {
                meanX /= divBy;
                meanY /= divBy;
                #ifdef DEBUG_DISPLAY
                    cv::circle( result, cv::Point(meanX+objects[obj].x+hMargin, meanY+objects[obj].y+vMargin), 1, cv::Scalar( 0, 0, 255 ), -1, 8 );
                #endif

                coords[obj*4+0] = meanX;
                coords[obj*4+1] = meanY;
                coords[obj*4+2] = cropCrop.width;
                coords[obj*4+3] = cropCrop.height;
            }
        }

        myfile << i;
        for (int j=0; j<coordSize; ++j)
        {
            myfile << ", " << coords[j];
        }
        myfile << "\n";

        #ifdef DEBUG_DISPLAY
            cv::imshow("Processed", result);
            
            cv::waitKey(1);
        #endif

        // auto t1 = std::chrono::high_resolution_clock::now();
        // auto dt = 1.e-9*std::chrono::duration_cast<std::chrono::nanoseconds>(t1-t0).count();
        // std::cerr << dt << "\n";
    }
    return 0;
}